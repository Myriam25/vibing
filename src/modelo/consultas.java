
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

public class consultas {
    // Atributo
    private Conexion conexion = new Conexion();
    
    //CONSULTAS
    public DefaultTableModel Consultar_Clientes(){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Clientes();");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
    public DefaultTableModel Consultar_Clientes_Especifico(int id){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Clientes_Especifico(" + id + ");");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
    public DefaultTableModel Consultar_Articulos(){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Articulos();");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
    public DefaultTableModel Consultar_Articulos_Especifico(int id){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Articulos_Especifico(" + id + ");");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
   
    public DefaultTableModel Consultar_Proveedores(){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Proveedores();");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
    public DefaultTableModel Consultar_Proveedores_Especifico(int id){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Proveedores_Especifico(" + id + ");");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
    
    public DefaultTableModel Consultar_Empleados(){
        try{
            Connection con = conexion.abrirConexion();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Empleados();");  
            DefaultTableModel dtm = new DefaultTableModel();    
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); // regresa el número de columnas
            for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
       }catch(SQLException e){
            return null;
        }
  }
    
    public DefaultTableModel Consultar_Empleados_Especifico(int id){
        try{
            Connection con = conexion.abrirConexion();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Empleados_Especifico(" + id + ");");  
            DefaultTableModel dtm = new DefaultTableModel();    
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); // regresa el número de columnas
            for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
       }catch(SQLException e){
            return null;
        }
  }
    public DefaultTableModel Consultar_Facturas(){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Facturas();");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}
  public DefaultTableModel Consultar_Facturas_Detalles(int id){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Facturas_Detalles(" + id + ");");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}

  public DefaultTableModel Consultar_Sucursales_Especifico(int id){
    try{
        Connection con = conexion.abrirConexion();
        Statement s = con.createStatement();
        ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Sucursales_Especifico(" + id + ");");  
        DefaultTableModel dtm = new DefaultTableModel();    
        ResultSetMetaData rsMd =  rs.getMetaData();
        int columnas = rsMd.getColumnCount(); // regresa el número de columnas
        for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
            dtm.addColumn(rsMd.getColumnLabel(i));
        }
        while(rs.next()){
            Object[] fila = new Object[columnas];
            for(int i=0; i< columnas; i++){
                fila[i] = rs.getObject(i+1);
            }
            dtm.addRow(fila);
        }
        return dtm;
   }catch(SQLException e){
        return null;
    }
}  
    
  public DefaultTableModel Consultar_Reporte_Venta(String fechaini,String fechafin){
        try{
            Connection con = conexion.abrirConexion();
            Statement s = con.createStatement();
            // call proyecto_tiendaropa.Consultar_Reporte_Venta('2021-01-01', '2021-01-30');
            ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Consultar_Reporte_Venta('"+ fechaini +"', '"+ fechafin +"');");  
            DefaultTableModel dtm = new DefaultTableModel();    
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); 
            for(int i=1; i <= columnas; i++){  
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
       }catch(SQLException e){
            return null;
        }
  }
  
  //Eliminar
  public boolean Eliminar_Clientes(int id){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Eliminar_Clientes("+ id +");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
  
  public boolean Eliminar_Empleados(int id){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Eliminar_Empleados("+ id +");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
  
  public boolean Eliminar_Articulos(int id){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Eliminar_Articulos("+ id +");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
  
  public boolean Eliminar_Proveedores(int id){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Eliminar_Proveedores("+ id +");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
  
  //insertar 
  public boolean Insertar_clientes(int id, String ubi, String telefono, String correo, int colonia){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Clientes("+null+",'"+ ubi +"','"+telefono+"','"+correo+"',"+colonia+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Insertar_clientes_nombre(String nombre1,String nombre2,String nombre3,String Apellido1, String Apellido2, int id ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call Insertar_Clientes_Nombre('" + nombre1 + "','" + nombre2 + "','" + nombre3 + "','" + Apellido1 + "','" + Apellido2 + "'," + id + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR2: "+e);
            return false;
        }
    }
    
    public boolean Insertar_Empleados(int id, String correo, String nss, String contrato, String ubi, String nacimiento, int dep, int colonia){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Empleados(" + id +",'"+ correo +"','"+ nss +"','"+ contrato +"','"+ ubi +"','"+ nacimiento +"'," + dep +"," + colonia + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROREMPLEADOS1: "+e);
            return false;
        }
    }
    
    public boolean Insertar_Empleados_Nombre(String nombre1,String nombre2,String nombre3,String Apellido1, String Apellido2, int id ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call Insertar_Empleados_Nombres('" + nombre1 + "','" + nombre2 + "','" + nombre3 + "','" + Apellido1 + "','" + Apellido2 + "'," + id + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROREMPLEADOS2: "+e);
            return false;
        }
    }
    public boolean Insertar_Empleados_Telefonos(String telefono,String tipo,int id ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call Insertar_Empleados_Telefonos('" + telefono + "','" + tipo + "'," + id + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROREMPLEADOS3: "+e);
            return false;
        }
    }
    
    public boolean Insertar_Productos(int id, String nombre, String desc, Float precio, int prov, int cat ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Articulos("+id+",'"+ nombre + "','" + desc + "'," + precio + "," + prov + "," +cat+ ");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Insertar_Proveedores(int id, String nombre,  String ubi, String correo, int colonia){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Proveedores("+id+",'"+ nombre +"','"+ ubi +"','"+correo+"',"+colonia+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Insertar_Proveedores_Telefonos(String telefono,String tipo, String prov, int id ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call Insertar_Proveedores_Telefonos('" + telefono + "','" + tipo + "','" + prov + "'," + id + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR2: "+e);
            return false;
        }
    }
    
    //modificar
    public boolean Modificar_Proveedores(int id, String ubi, String correo, int colonia){
        try{
            //in id int, in ubi varchar(30), in correo varchar(45), in colonia int
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Modificar_Proveedores("+id+",'"+ ubi +"','"+correo+"',"+colonia+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Modificar_Clientes(int id, String ubi,String telefono, String correo, int colonia){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Modificar_Clientes("+id+",'"+ ubi +"','"+telefono+"','"+correo+"',"+colonia+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Modificar_Empleados(int id, String correo,String ubi, int dep, int colonia){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
            //IN `id` INT, IN `correo` VARCHAR(45), IN `nss` VARCHAR(15), IN `ubi` VARCHAR(45), IN `depa` INT, IN `colonia` INT
        String cadenaSQL =  "call proyecto_tiendaropa.Modificar_Empleados(" + id +",'"+ correo +"','"+ ubi +"',"+ dep +"," + colonia + ");"; 
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
    public boolean Modificar_Articulos(int id, Float precio, int prov){
        try{
            //in id int, in ubi varchar(30), in correo varchar(45), in colonia int
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Modificar_Articulos("+id+","+ precio +","+prov+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    
  
  
  // Funciones
  
  public DefaultTableModel Reportes_Venta_Total(String fechaini,String fechafin){
        try{
            Connection con = conexion.abrirConexion();
            Statement s = con.createStatement();
            // call proyecto_tiendaropa.Consultar_Reporte_Venta('2021-01-01', '2021-01-30');
            ResultSet rs = s.executeQuery("call proyecto_tiendaropa.Reportes_Venta_Total('"+ fechaini +"', '"+ fechafin +"');");  
            DefaultTableModel dtm = new DefaultTableModel();    
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); 
            for(int i=1; i <= columnas; i++){  
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
       }catch(SQLException e){
            return null;
        }
       
  }
    public DefaultTableModel Bono(String fechaini,String fechafin){
        try{
            Connection con = conexion.abrirConexion();
            Statement s = con.createStatement();
            // call proyecto_tiendaropa.Consultar_Reporte_Venta('2021-01-01', '2021-01-30');
            ResultSet rs = s.executeQuery("select bono('"+ fechaini +"', '"+ fechafin +"') as 'Bono';");  
            DefaultTableModel dtm = new DefaultTableModel();    
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); 
            for(int i=1; i <= columnas; i++){  
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
       }catch(SQLException e){
            return null;
        }   
  }
    public boolean Insertar_Sucursal_Inventario(int suc, int cant, int art, int talla ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Sucursal_Inventario(" + suc +"," + cant +","+ art + "," + talla + ");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }
    public boolean Insertar_Facturas(int id,String fechahoy ,int idclientes, int idempleado, int idmetodo){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Facturas("+id+",'"+ fechahoy +"',"+idclientes+","+idempleado+","+idmetodo+");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }

public boolean Insertar_Detalles_Facturas(int cant, float precio, int factura, int art ){
        try{
            Connection con = conexion.abrirConexion();            
            Statement s = con.createStatement();
            //IN `cant` INT, IN `precio` FLOAT(2), IN `factura` INT, IN `art` INT
        String cadenaSQL =  "call proyecto_tiendaropa.Insertar_Detalles_Factura(" + cant +"," + precio +","+ factura + "," + art + ");";
            int registro = s.executeUpdate(cadenaSQL);           
            conexion.cerrarConexion(con);
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e);
            return false;
        }
    }

}

