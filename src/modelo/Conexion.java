/*
 * KARLA MARÍA HERRERA VALDÉS
 * CONEXION A LA BASE DE DATOS
 * 
 */
package modelo;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Conexion {
    public Connection abrirConexion() throws SQLException{
        Connection con;
        //Para conectarnos a nuestra base de datos
       try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/proyecto_tiendaropa","root", "123"); // Se establece la conexión
        }catch(SQLException e){
            System.out.println("NO se pudo abrir conexión");            
            con = null;
        }            
        return con;
    }
    
    public void cerrarConexion(Connection c) throws SQLException{        
        try{
            if(!c.isClosed()){
                c.close();
            }
        }catch(SQLException e){
            System.out.println("Error al cerrar la conexión");
        }        
    }
}
