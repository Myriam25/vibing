package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.consultas;
import vista.ReporteDeVentas;
import vista.AgregarProductos;
import vista.AgregarProveedores;
import vista.IniciarSesion;
import vista.PaginaPrincipal;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;



public class ControladorReporteDeVenta implements ActionListener{
   private consultas modelo;
   private ReporteDeVentas vista;
   //// crear variable de principal empleados
   private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
   
    public ControladorReporteDeVenta(consultas modelo,ReporteDeVentas vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnConsultar.addActionListener(this);
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
    }


    public void iniciarVista() {
        vista.setTitle("REPORTES DE VENTAS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent evento) {
             if(vista.btnConsultar == evento.getSource()) {
              Date feci = vista.dateinicio.getDate();
              Date fecf = vista.datefin.getDate();
              //Date dateFromDateChooser = dateChooser.getDate();
              String fecini = String.format("%1$tY-%1$tm-%1$td", feci);  
              String fecfin = String.format("%1$tY-%1$tm-%1$td", fecf); 
              this.vista.tbconsulta.setModel(modelo.Consultar_Reporte_Venta(fecini,fecfin));   
              this.vista.tbtotal.setModel(modelo.Reportes_Venta_Total(fecini, fecfin));   
              this.vista.tbBono.setModel(modelo.Bono(fecini, fecfin));  
        }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
}
