/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.consultas;
import vista.IniciarSesion;
import vista.ModificarFacturas;
import vista.ModificarProductos;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorModificarProductos implements ActionListener{
    private consultas modelo;
    private ModificarProductos vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    public int prov;


    public ControladorModificarProductos(consultas modelo,ModificarProductos vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        
    }
    public void iniciarVista() {
        vista.setTitle("MODIFICAR PRODUCTOS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void proveedores(){
         if ( vista.cbproovedores.getSelectedItem()== "Moda Suprema"){
             prov=1;
         }else if(vista.cbproovedores.getSelectedItem()== "Just Clothes"){
             prov=2;
         }else if(vista.cbproovedores.getSelectedItem()== "Kids a la moda"){
             prov=3;
         }else if(vista.cbproovedores.getSelectedItem()== "Loyalty Clothes"){
             prov=4;
         }else{
             prov=5;
         }  
     }

    @Override
    public void actionPerformed(ActionEvent evento) {
        if(vista.btnGuardar == evento.getSource()) {
                try{
                    proveedores();
                modelo.Modificar_Articulos(Integer.parseInt(vista.txtID.getText()),Float.parseFloat(vista.txtPrecio.getText()),prov );
               JOptionPane.showMessageDialog(null, "Registro insertado exitosamente");
                }catch(Exception e){
                    System.out.println("Error:"+e);
                    JOptionPane.showMessageDialog(null, "No se pudo insertar");
                }
            }else if(vista.btnCancelar == evento.getSource()) {
                try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
            }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
}
