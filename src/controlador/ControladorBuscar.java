/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import modelo.consultas;
import vista.AgregarProveedores;
import vista.Buscar;
import vista.PuntoDeVenta;

/**
 *
 * @author Paola
 */
public class ControladorBuscar implements ActionListener{
    private consultas modelo;
    private Buscar vista;
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();

    public ControladorBuscar(consultas modelo, Buscar vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnAnterior.addActionListener(this);
    }
    public void iniciarVista() {
        vista.setTitle("BUSCAR");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent evento) {
        if(vista.btnAnterior == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
        }
    }
}
