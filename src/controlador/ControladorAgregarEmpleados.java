/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.consultas;
import vista.AgregarEmpleados;
import vista.IniciarSesion;
import vista.ModificarEmpleados;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorAgregarEmpleados implements ActionListener{
    private consultas modelo;
    private AgregarEmpleados vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    public int departa;

    public ControladorAgregarEmpleados(consultas modelo, AgregarEmpleados vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir.addActionListener(this);
        this.vista.btnguardar.addActionListener(this);
    }
     public void iniciarVista() {
        vista.setTitle("AGREGAR EMPLEADOS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
     public void departamentos(){
         if ( vista.cbdepartamentos.getSelectedItem()== "Administración"){
             departa=1;
         }else if(vista.cbdepartamentos.getSelectedItem()== "Recursos Humanos"){
             departa=2;
         }else if(vista.cbdepartamentos.getSelectedItem()== "Ventas"){
             departa=3;
         }else if(vista.cbdepartamentos.getSelectedItem()== "marketing"){
             departa=4;
         }else{
             departa=5;
         }  
     }
     
    @Override
    public void actionPerformed(ActionEvent evento) {
            if(vista.btnguardar == evento.getSource()) {
                try{
                    departamentos();
                Date feccon = vista.datecontrato.getDate();
                Date fecnac = vista.datenacimiento.getDate();
                String fechacon = String.format("%1$tY-%1$tm-%1$td", feccon);  
                String fechanac = String.format("%1$tY-%1$tm-%1$td", fecnac);
                modelo.Insertar_Empleados(Integer.parseInt(vista.txtID.getText()), vista.txtCorreos.getText(), vista.txtNss.getText(),fechacon, vista.txtDireccion.getText(),fechanac,departa,Integer.parseInt(vista.txtidcolonia.getText()));
               modelo.Insertar_Empleados_Nombre(vista.txtnombre1.getText(), vista.txtnombre2.getText(), vista.txtnombre3.getText(), vista.txtApellido1.getText(), vista.txtApellido2.getText(),Integer.parseInt(vista.txtID.getText()));
               String tipotelefono=vista.cbtelefono.getSelectedItem().toString();
               modelo.Insertar_Empleados_Telefonos(vista.txtTelefonosempleados.getText(),tipotelefono , Integer.parseInt(vista.txtID.getText()));
               JOptionPane.showMessageDialog(null, "Registro insertado exitosamente");
                }catch(Exception e){
                    System.out.println("Error:"+e);
                    
                }
            }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
              }else if (vista.btnSalir == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
}
