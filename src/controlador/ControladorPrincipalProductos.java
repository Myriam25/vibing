/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.AgregarProductos;
import vista.IniciarSesion;
import vista.ModificarProductos;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorPrincipalProductos implements ActionListener{
    private consultas modelo;
    private PrincipalProductos vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    private AgregarProductos vistaagregarproductos= new AgregarProductos();
    private ModificarProductos vistamodificarproductos = new ModificarProductos();

    public ControladorPrincipalProductos(consultas modelo, PrincipalProductos vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.agregarproductosbtn.addActionListener(this);
        this.vista.modificarproductosbtn.addActionListener(this);
        this.vista.limpiarproductosbtn.addActionListener(this);
        this.vista.buscarproductosbtn.addActionListener(this);
        this.vista.todosproductosbtn.addActionListener(this);
        this.vista.eliminarproductosbtn.addActionListener(this);
    }
    public void iniciarVista() {
        vista.setTitle("PRINCIPAL PRODUCTOS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void limpiarCajasTexto(){
        vista.buscarproductoslbl.setText("");
        vista.productostbt.setModel(new DefaultTableModel());
    }
    
    
    @Override
    public void actionPerformed(ActionEvent evento) {
                if(vista.eliminarproductosbtn == evento.getSource()){
                    try{
                        modelo.Eliminar_Articulos(Integer.parseInt(vista.buscarproductoslbl.getText())); 
                        JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null, "No se pudo eliminar el registro");
                    }
                }else if(vista.limpiarproductosbtn == evento.getSource()){//limpiar
                        limpiarCajasTexto();

               }else if(vista.todosproductosbtn == evento.getSource()){//todos
                    modelo.Consultar_Articulos();
                    this.vista.productostbt.setModel(modelo.Consultar_Articulos());
                    
               }else if(vista.buscarproductosbtn == evento.getSource()){//buscar especifico
                   this.vista.productostbt.setModel(modelo.Consultar_Sucursales_Especifico(Integer.parseInt(vista.buscarproductoslbl.getText()))); 
                   
               }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.agregarproductosbtn == evento.getSource()){
                   try{
                        ControladorAgregarProductos cpe = new ControladorAgregarProductos(modelo, vistaagregarproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.modificarproductosbtn == evento.getSource()){
                   try{
                        ControladorModificarProductos cpe = new ControladorModificarProductos(modelo, vistamodificarproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
}
