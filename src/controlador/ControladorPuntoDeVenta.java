/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.IniciarSesion;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorPuntoDeVenta implements ActionListener{
    private consultas modelo;
    private PuntoDeVenta vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    public int ta;

    public ControladorPuntoDeVenta(consultas modelo, PuntoDeVenta vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.btncargardatos.addActionListener(this);
        this.vista.btnagregar.addActionListener(this);
        this.vista.btnvaciar.addActionListener(this);
        this.vista.btnpagar.addActionListener(this);
        }
    public void iniciarVista() {
        vista.setTitle("PUNTO DE VENTA");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void metodo(){
         if ( vista.cbmetodo.getSelectedItem()== "Efectivo"){
             ta=1;
         }else if(vista.cbmetodo.getSelectedItem()== "Débito"){
             ta=2;
         }else if(vista.cbmetodo.getSelectedItem()== "Crédito"){
             ta=3;
         }else if(vista.cbmetodo.getSelectedItem()== "Cheque"){
             ta=4;
         }else{
             ta=5;
         }  
     }
    public void vaciar(){
        vista.txtbuscar3.setText("");
        vista.txtcantidad.setText("");
        vista.txtidclientes.setText("");
        vista.txtidempleado.setText("");
        vista.txtidfacturas.setText("");
        vista.txtbuscar1.setText("");
        vista.tbproductos.setModel(new DefaultTableModel());
    }
    public void limpiar(){
        vista.txtbuscar3.setText("");
        vista.txtcantidad.setText("");
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        if(vista.btncargardatos== evento.getSource()){
                    try{
                        metodo();
                        Date feci = vista.datehoy.getDate();
                        String fecini = String.format("%1$tY-%1$tm-%1$td", feci); 
                        modelo.Insertar_Facturas(Integer.parseInt(vista.txtidfacturas.getText()), fecini, Integer.parseInt(vista.txtidclientes.getText()), Integer.parseInt(vista.txtidempleado.getText()), ta);
                        JOptionPane.showMessageDialog(null, "Registro insertado exitosamente");
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnagregar== evento.getSource()){
                    try{
             
                    modelo.Insertar_Detalles_Facturas(Integer.parseInt(vista.txtcantidad.getText()), 0, Integer.parseInt(vista.txtidfacturas.getText()), Integer.parseInt(vista.txtbuscar3.getText()));
                    JOptionPane.showMessageDialog(null, "Registro insertado exitosamente");
                    modelo.Consultar_Facturas_Detalles(Integer.parseInt(vista.txtidfacturas.getText()));
                    this.vista.tbproductos.setModel(modelo.Consultar_Facturas_Detalles(Integer.parseInt(vista.txtidfacturas.getText())));
                    limpiar();
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnvaciar == evento.getSource()){
                   vaciar();
                   
               }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
    
}
