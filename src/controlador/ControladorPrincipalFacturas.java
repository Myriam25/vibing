/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.IniciarSesion;
import vista.ModificarFacturas;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorPrincipalFacturas implements ActionListener{
    private consultas modelo;
    private PrincipalFacturas vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    private ModificarFacturas vistamodificarfacturas = new ModificarFacturas();

    public ControladorPrincipalFacturas(consultas modelo, PrincipalFacturas vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos2.addActionListener(this);
        this.vista.btnProveedores2.addActionListener(this);
        this.vista.btnPuntoVenta2.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.agregarfacturasbtn.addActionListener(this);
        this.vista.modificarfacturasbtn.addActionListener(this);
        this.vista.limpiarfacturasbtn.addActionListener(this);
        this.vista.modificarfacturasbtn.addActionListener(this);
        this.vista.buscarfacturasbtn.addActionListener(this);
        this.vista.todosfacturasbtn.addActionListener(this);
    }
    public void iniciarVista() {
        vista.setTitle("PRINCIPAL FACTURAS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void limpiarCajasTexto(){
        vista.buscarfacturaslbl.setText("");
        vista.facturatb.setModel(new DefaultTableModel());
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
               if(vista.limpiarfacturasbtn == evento.getSource()){//limpiar
                        limpiarCajasTexto();

               }else if(vista.buscarfacturasbtn == evento.getSource()){//buscar especifico
                   this.vista.facturatb.setModel(modelo.Consultar_Facturas_Detalles(Integer.parseInt(vista.buscarfacturaslbl.getText()))); 
                   
               }else if(vista.todosfacturasbtn == evento.getSource()) {//todos los clientes
                        modelo.Consultar_Facturas();
                        this.vista.facturatb.setModel(modelo.Consultar_Facturas());      
               }else if(vista.btnPuntoVenta2 == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos2 == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores2 == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.modificarfacturasbtn == evento.getSource()){
                   try{
                        ControladorModificarFacturas cpe = new ControladorModificarFacturas(modelo, vistamodificarfacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    
    
    
}
}
