/*
 * Autor: Karla María Herrera Valdés
 * Fecha:25/06/22
 * Descripción: Ventana para reporte de ventas
 */
package vista;

public class ReporteDeVentas extends javax.swing.JFrame {

    public ReporteDeVentas() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        iconFecha = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblsucursal = new javax.swing.JLabel();
        lblSucursal = new javax.swing.JLabel();
        cBoxsucursal = new javax.swing.JComboBox();
        lblfechadeinicio = new javax.swing.JLabel();
        lblfechadefin = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbconsulta = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnPuntoVenta = new javax.swing.JButton();
        btnProductos = new javax.swing.JButton();
        btnProveedores = new javax.swing.JButton();
        btnClientes = new javax.swing.JButton();
        btnRecursosHumanos = new javax.swing.JButton();
        btnReportesVenta = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        iconPuntoVenta = new javax.swing.JLabel();
        iconProductos = new javax.swing.JLabel();
        iconProveedores = new javax.swing.JLabel();
        iconClientes = new javax.swing.JLabel();
        iconRecursosHumanos = new javax.swing.JLabel();
        iconReportesVenta = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        lblContacto = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        iconUsuario = new javax.swing.JLabel();
        iconLogoTienda1 = new javax.swing.JLabel();
        dateinicio = new com.toedter.calendar.JDateChooser();
        datefin = new com.toedter.calendar.JDateChooser();
        btnConsultar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbtotal = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbBono = new javax.swing.JTable();
        btnSalir1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Reportedeventa");
        setMinimumSize(new java.awt.Dimension(1900, 1000));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMaximumSize(new java.awt.Dimension(1900, 1000));
        jPanel1.setPreferredSize(new java.awt.Dimension(1900, 1000));

        lblUsuario.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblUsuario.setForeground(new java.awt.Color(102, 102, 102));
        lblUsuario.setText("Root");

        iconFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/calendario.png"))); // NOI18N

        lblFecha.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblFecha.setForeground(new java.awt.Color(102, 102, 102));
        lblFecha.setText("26 / 06 / 2022");

        lblsucursal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblsucursal.setText("Sucursal:");

        lblSucursal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblSucursal.setForeground(new java.awt.Color(102, 102, 102));
        lblSucursal.setText("Suc. Gran Plaza");

        cBoxsucursal.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        cBoxsucursal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sucursal Centro", "Sucursal Gran Plaza", "Sucursal Marina" }));
        cBoxsucursal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        cBoxsucursal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cBoxsucursalActionPerformed(evt);
            }
        });

        lblfechadeinicio.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblfechadeinicio.setText("Fecha de inicio: ");

        lblfechadefin.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblfechadefin.setText("Fecha de fin: ");

        tbconsulta.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        tbconsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbconsulta);

        jPanel2.setBackground(new java.awt.Color(6, 56, 82));

        btnPuntoVenta.setBackground(new java.awt.Color(6, 56, 82));
        btnPuntoVenta.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnPuntoVenta.setForeground(new java.awt.Color(255, 255, 255));
        btnPuntoVenta.setText("Punto de venta");
        btnPuntoVenta.setBorder(null);
        btnPuntoVenta.setContentAreaFilled(false);
        btnPuntoVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnPuntoVenta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnPuntoVenta.setOpaque(true);
        btnPuntoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPuntoVentaActionPerformed(evt);
            }
        });

        btnProductos.setBackground(new java.awt.Color(6, 56, 82));
        btnProductos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnProductos.setForeground(new java.awt.Color(255, 255, 255));
        btnProductos.setText("Productos");
        btnProductos.setBorder(null);
        btnProductos.setContentAreaFilled(false);
        btnProductos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnProductos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnProductos.setOpaque(true);
        btnProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductosActionPerformed(evt);
            }
        });

        btnProveedores.setBackground(new java.awt.Color(6, 56, 82));
        btnProveedores.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnProveedores.setForeground(new java.awt.Color(255, 255, 255));
        btnProveedores.setText("Proveedores");
        btnProveedores.setBorder(null);
        btnProveedores.setContentAreaFilled(false);
        btnProveedores.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnProveedores.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnProveedores.setOpaque(true);
        btnProveedores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProveedoresActionPerformed(evt);
            }
        });

        btnClientes.setBackground(new java.awt.Color(6, 56, 82));
        btnClientes.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnClientes.setForeground(new java.awt.Color(255, 255, 255));
        btnClientes.setText("Clientes");
        btnClientes.setBorder(null);
        btnClientes.setContentAreaFilled(false);
        btnClientes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnClientes.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnClientes.setOpaque(true);
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        btnRecursosHumanos.setBackground(new java.awt.Color(6, 56, 82));
        btnRecursosHumanos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnRecursosHumanos.setForeground(new java.awt.Color(255, 255, 255));
        btnRecursosHumanos.setText("Recursos Humanos");
        btnRecursosHumanos.setBorder(null);
        btnRecursosHumanos.setContentAreaFilled(false);
        btnRecursosHumanos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnRecursosHumanos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnRecursosHumanos.setOpaque(true);
        btnRecursosHumanos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecursosHumanosActionPerformed(evt);
            }
        });

        btnReportesVenta.setBackground(new java.awt.Color(6, 56, 82));
        btnReportesVenta.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnReportesVenta.setForeground(new java.awt.Color(255, 255, 255));
        btnReportesVenta.setText("Reportes de venta");
        btnReportesVenta.setBorder(null);
        btnReportesVenta.setContentAreaFilled(false);
        btnReportesVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnReportesVenta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnReportesVenta.setOpaque(true);
        btnReportesVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportesVentaActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(6, 56, 82));
        jButton7.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText("Facturación");
        jButton7.setBorder(null);
        jButton7.setContentAreaFilled(false);
        jButton7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton7.setOpaque(true);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        iconPuntoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Punto de venta.png"))); // NOI18N

        iconProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Productos.png"))); // NOI18N

        iconProveedores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Proveedores.png"))); // NOI18N

        iconClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Clientes.png"))); // NOI18N

        iconRecursosHumanos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Empleados.png"))); // NOI18N

        iconReportesVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Reportes de ventas.png"))); // NOI18N

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/factura.png"))); // NOI18N

        lblTelefono.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblTelefono.setForeground(new java.awt.Color(255, 255, 255));
        lblTelefono.setText("Telefono: 669-123-4567");

        lblContacto.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblContacto.setForeground(new java.awt.Color(255, 255, 255));
        lblContacto.setText("Contacto");

        lblCorreo.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblCorreo.setForeground(new java.awt.Color(255, 255, 255));
        lblCorreo.setText("Correo: boutiquevibing@contacto.com");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconPuntoVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnPuntoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconProductos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconProveedores)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconClientes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconRecursosHumanos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRecursosHumanos, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconReportesVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnReportesVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(107, 107, 107)
                        .addComponent(lblContacto))
                    .addComponent(lblCorreo)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(lblTelefono)
                        .addGap(44, 44, 44)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(118, 118, 118)
                        .addComponent(btnPuntoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(iconPuntoVenta)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(iconProductos)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(iconProveedores)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(iconClientes)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRecursosHumanos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(iconRecursosHumanos)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnReportesVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(iconReportesVenta)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel12)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addComponent(lblContacto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblTelefono)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCorreo)
                .addGap(102, 102, 102))
        );

        iconUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/usuario (4).png"))); // NOI18N

        iconLogoTienda1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/LOGO (9).png"))); // NOI18N

        dateinicio.setDateFormatString("yyyy-MM-dd");

        datefin.setDateFormatString("yyyy-MM-dd");

        btnConsultar.setBackground(new java.awt.Color(240, 129, 15));
        btnConsultar.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnConsultar.setForeground(new java.awt.Color(255, 255, 255));
        btnConsultar.setText("Consultar");

        tbtotal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        tbtotal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tbtotal);

        tbBono.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        tbBono.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tbBono);

        btnSalir1.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir1.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnSalir1.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/cerrar-sesion (1).png"))); // NOI18N
        btnSalir1.setActionCommand("buscar");
        btnSalir1.setAlignmentX(0.5F);
        btnSalir1.setBorder(null);
        btnSalir1.setContentAreaFilled(false);
        btnSalir1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir1.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(iconUsuario)
                .addGap(18, 18, 18)
                .addComponent(lblUsuario)
                .addGap(45, 45, 45)
                .addComponent(btnSalir1)
                .addGap(1120, 1120, 1120)
                .addComponent(lblSucursal)
                .addGap(65, 65, 65)
                .addComponent(iconFecha)
                .addGap(8, 8, 8)
                .addComponent(lblFecha)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(116, 116, 116)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1281, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblfechadefin)
                                    .addComponent(datefin, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(1161, 1161, 1161)
                                .addComponent(btnConsultar)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(338, 338, 338)
                                        .addComponent(lblsucursal)
                                        .addGap(44, 44, 44)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(iconLogoTienda1)
                                            .addComponent(cBoxsucursal, javax.swing.GroupLayout.PREFERRED_SIZE, 613, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(dateinicio, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(290, 290, 290))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(lblfechadeinicio)
                                .addGap(112, 112, 112))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconUsuario)
                    .addComponent(lblUsuario)
                    .addComponent(iconFecha)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSucursal)
                            .addComponent(lblFecha)))
                    .addComponent(btnSalir1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(iconLogoTienda1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(69, 69, 69)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblsucursal)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(cBoxsucursal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(48, 48, 48)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblfechadeinicio, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblfechadefin, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(dateinicio, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(datefin, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1884, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 953, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 47, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProveedoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProveedoresActionPerformed
       
    }//GEN-LAST:event_btnProveedoresActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        
    }//GEN-LAST:event_btnClientesActionPerformed

    private void btnReportesVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportesVentaActionPerformed
       
    }//GEN-LAST:event_btnReportesVentaActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
      
    }//GEN-LAST:event_jButton7ActionPerformed

    private void cBoxsucursalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cBoxsucursalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cBoxsucursalActionPerformed

    private void btnPuntoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPuntoVentaActionPerformed
        
    }//GEN-LAST:event_btnPuntoVentaActionPerformed

    private void btnProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductosActionPerformed
        
    }//GEN-LAST:event_btnProductosActionPerformed

    private void btnRecursosHumanosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecursosHumanosActionPerformed
      
    }//GEN-LAST:event_btnRecursosHumanosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReporteDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReporteDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReporteDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReporteDeVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReporteDeVentas().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnClientes;
    public javax.swing.JButton btnConsultar;
    public javax.swing.JButton btnProductos;
    public javax.swing.JButton btnProveedores;
    public javax.swing.JButton btnPuntoVenta;
    public javax.swing.JButton btnRecursosHumanos;
    public javax.swing.JButton btnReportesVenta;
    public javax.swing.JButton btnSalir1;
    private javax.swing.JComboBox cBoxsucursal;
    public com.toedter.calendar.JDateChooser datefin;
    public com.toedter.calendar.JDateChooser dateinicio;
    private javax.swing.JLabel iconClientes;
    private javax.swing.JLabel iconFecha;
    private javax.swing.JLabel iconLogoTienda1;
    private javax.swing.JLabel iconProductos;
    private javax.swing.JLabel iconProveedores;
    private javax.swing.JLabel iconPuntoVenta;
    private javax.swing.JLabel iconRecursosHumanos;
    private javax.swing.JLabel iconReportesVenta;
    private javax.swing.JLabel iconUsuario;
    public javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblContacto;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblSucursal;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblfechadefin;
    private javax.swing.JLabel lblfechadeinicio;
    private javax.swing.JLabel lblsucursal;
    public javax.swing.JTable tbBono;
    public javax.swing.JTable tbconsulta;
    public javax.swing.JTable tbtotal;
    // End of variables declaration//GEN-END:variables
}
