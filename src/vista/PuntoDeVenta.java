/*
 * Autor: Karla María Herrera Valdés
 * Fecha:25/06/22
 * Descripción: Ventana para punto de venta.
 */
package vista;

public class PuntoDeVenta extends javax.swing.JFrame {

    public PuntoDeVenta() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        iconFecha = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblSucursal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnPuntoVenta = new javax.swing.JButton();
        btnProductos = new javax.swing.JButton();
        btnProveedores = new javax.swing.JButton();
        btnClientes = new javax.swing.JButton();
        btnRecursosHumanos = new javax.swing.JButton();
        btnReportesVenta = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        iconPuntoVenta = new javax.swing.JLabel();
        iconProductos = new javax.swing.JLabel();
        iconProveedores = new javax.swing.JLabel();
        iconClientes = new javax.swing.JLabel();
        iconRecursosHumanos = new javax.swing.JLabel();
        iconReportesVenta = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        lblContacto = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        iconUsuario = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnvaciar = new javax.swing.JButton();
        lbltotal = new javax.swing.JLabel();
        lbltotalfinal = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbproductos = new javax.swing.JTable();
        lblproductos = new javax.swing.JLabel();
        txtidclientes = new javax.swing.JTextField();
        btnagregar = new javax.swing.JButton();
        cBoxsucursal = new javax.swing.JComboBox();
        lblsucursal = new javax.swing.JLabel();
        lblclientes = new javax.swing.JLabel();
        btnpagar = new javax.swing.JButton();
        lblEmpleado = new javax.swing.JLabel();
        lblclientes1 = new javax.swing.JLabel();
        cbmetodo = new javax.swing.JComboBox();
        lbltotal1 = new javax.swing.JLabel();
        txtbuscar1 = new javax.swing.JTextField();
        lbltotal2 = new javax.swing.JLabel();
        lbltotalfinal2 = new javax.swing.JLabel();
        txtidempleado = new javax.swing.JTextField();
        txtbuscar3 = new javax.swing.JTextField();
        lblclientes2 = new javax.swing.JLabel();
        txtidfacturas = new javax.swing.JTextField();
        btncargardatos = new javax.swing.JButton();
        lblclientes3 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JTextField();
        iconLogoTienda1 = new javax.swing.JLabel();
        btnSalir1 = new javax.swing.JButton();
        datehoy = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Punto de venta");
        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMaximumSize(new java.awt.Dimension(1900, 1000));
        jPanel1.setPreferredSize(new java.awt.Dimension(1900, 1000));

        lblUsuario.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblUsuario.setForeground(new java.awt.Color(102, 102, 102));
        lblUsuario.setText("Root");

        iconFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/calendario.png"))); // NOI18N

        lblFecha.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblFecha.setForeground(new java.awt.Color(102, 102, 102));
        lblFecha.setText("26 / 06 / 2022");

        lblSucursal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblSucursal.setForeground(new java.awt.Color(102, 102, 102));
        lblSucursal.setText("Suc. Gran Plaza");

        jPanel2.setBackground(new java.awt.Color(6, 56, 82));

        btnPuntoVenta.setBackground(new java.awt.Color(6, 56, 82));
        btnPuntoVenta.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnPuntoVenta.setForeground(new java.awt.Color(255, 255, 255));
        btnPuntoVenta.setText("Punto de venta");
        btnPuntoVenta.setBorder(null);
        btnPuntoVenta.setContentAreaFilled(false);
        btnPuntoVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnPuntoVenta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnPuntoVenta.setOpaque(true);
        btnPuntoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPuntoVentaActionPerformed(evt);
            }
        });

        btnProductos.setBackground(new java.awt.Color(6, 56, 82));
        btnProductos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnProductos.setForeground(new java.awt.Color(255, 255, 255));
        btnProductos.setText("Productos");
        btnProductos.setBorder(null);
        btnProductos.setContentAreaFilled(false);
        btnProductos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnProductos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnProductos.setOpaque(true);
        btnProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductosActionPerformed(evt);
            }
        });

        btnProveedores.setBackground(new java.awt.Color(6, 56, 82));
        btnProveedores.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnProveedores.setForeground(new java.awt.Color(255, 255, 255));
        btnProveedores.setText("Proveedores");
        btnProveedores.setBorder(null);
        btnProveedores.setContentAreaFilled(false);
        btnProveedores.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnProveedores.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnProveedores.setOpaque(true);
        btnProveedores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProveedoresActionPerformed(evt);
            }
        });

        btnClientes.setBackground(new java.awt.Color(6, 56, 82));
        btnClientes.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnClientes.setForeground(new java.awt.Color(255, 255, 255));
        btnClientes.setText("Clientes");
        btnClientes.setBorder(null);
        btnClientes.setContentAreaFilled(false);
        btnClientes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnClientes.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnClientes.setOpaque(true);
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        btnRecursosHumanos.setBackground(new java.awt.Color(6, 56, 82));
        btnRecursosHumanos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnRecursosHumanos.setForeground(new java.awt.Color(255, 255, 255));
        btnRecursosHumanos.setText("Recursos Humanos");
        btnRecursosHumanos.setBorder(null);
        btnRecursosHumanos.setContentAreaFilled(false);
        btnRecursosHumanos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnRecursosHumanos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnRecursosHumanos.setOpaque(true);
        btnRecursosHumanos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecursosHumanosActionPerformed(evt);
            }
        });

        btnReportesVenta.setBackground(new java.awt.Color(6, 56, 82));
        btnReportesVenta.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnReportesVenta.setForeground(new java.awt.Color(255, 255, 255));
        btnReportesVenta.setText("Reportes de venta");
        btnReportesVenta.setBorder(null);
        btnReportesVenta.setContentAreaFilled(false);
        btnReportesVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnReportesVenta.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnReportesVenta.setOpaque(true);
        btnReportesVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportesVentaActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(6, 56, 82));
        jButton7.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText("Facturación");
        jButton7.setBorder(null);
        jButton7.setContentAreaFilled(false);
        jButton7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton7.setOpaque(true);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        iconPuntoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Punto de venta.png"))); // NOI18N

        iconProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Productos.png"))); // NOI18N

        iconProveedores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Proveedores.png"))); // NOI18N

        iconClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Clientes.png"))); // NOI18N

        iconRecursosHumanos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Empleados.png"))); // NOI18N

        iconReportesVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Reportes de ventas.png"))); // NOI18N

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/factura.png"))); // NOI18N

        lblTelefono.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblTelefono.setForeground(new java.awt.Color(255, 255, 255));
        lblTelefono.setText("Telefono: 669-123-4567");

        lblContacto.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblContacto.setForeground(new java.awt.Color(255, 255, 255));
        lblContacto.setText("Contacto");

        lblCorreo.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblCorreo.setForeground(new java.awt.Color(255, 255, 255));
        lblCorreo.setText("Correo: boutiquevibing@contacto.com");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconPuntoVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnPuntoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconProductos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconProveedores)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconClientes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconRecursosHumanos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRecursosHumanos, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(iconReportesVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnReportesVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(107, 107, 107)
                        .addComponent(lblContacto))
                    .addComponent(lblCorreo)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(lblTelefono)
                        .addGap(44, 44, 44)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(iconPuntoVenta)
                        .addGap(27, 27, 27))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnPuntoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(iconProductos)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(iconProveedores)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(iconClientes)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRecursosHumanos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(iconRecursosHumanos)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnReportesVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(iconReportesVenta)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel12)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                .addComponent(lblContacto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblTelefono)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCorreo)
                .addGap(114, 114, 114))
        );

        iconUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/usuario (4).png"))); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        btnvaciar.setBackground(new java.awt.Color(240, 129, 15));
        btnvaciar.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnvaciar.setForeground(new java.awt.Color(255, 255, 255));
        btnvaciar.setText("Vaciar(F2)");
        btnvaciar.setActionCommand("buscar");
        btnvaciar.setAlignmentX(0.5F);
        btnvaciar.setBorder(null);
        btnvaciar.setContentAreaFilled(false);
        btnvaciar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnvaciar.setOpaque(true);
        btnvaciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvaciarActionPerformed(evt);
            }
        });

        lbltotal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbltotal.setText("Total : ");

        lbltotalfinal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbltotalfinal.setText("0.0");

        tbproductos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        tbproductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbproductos);

        lblproductos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblproductos.setText("Productos: ");

        txtidclientes.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtidclientes.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtidclientes.setMinimumSize(new java.awt.Dimension(28, 11));
        txtidclientes.setPreferredSize(new java.awt.Dimension(11, 28));

        btnagregar.setBackground(new java.awt.Color(240, 129, 15));
        btnagregar.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnagregar.setForeground(new java.awt.Color(255, 255, 255));
        btnagregar.setText("Agregar");
        btnagregar.setActionCommand("buscar");
        btnagregar.setAlignmentX(0.5F);
        btnagregar.setBorder(null);
        btnagregar.setContentAreaFilled(false);
        btnagregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnagregar.setOpaque(true);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        cBoxsucursal.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        cBoxsucursal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sucursal Centro", "Sucursal Gran Plaza", "Sucursal Marina" }));
        cBoxsucursal.setBorder(null);
        cBoxsucursal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cBoxsucursalActionPerformed(evt);
            }
        });

        lblsucursal.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblsucursal.setText("Sucursal:");

        lblclientes.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblclientes.setText("ID Clientes:");
        lblclientes.setAlignmentX(0.5F);
        lblclientes.setAlignmentY(0.0F);

        btnpagar.setBackground(new java.awt.Color(240, 129, 15));
        btnpagar.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnpagar.setForeground(new java.awt.Color(255, 255, 255));
        btnpagar.setText("Pagar (F1)");
        btnpagar.setActionCommand("buscar");
        btnpagar.setAlignmentX(0.5F);
        btnpagar.setBorder(null);
        btnpagar.setContentAreaFilled(false);
        btnpagar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnpagar.setOpaque(true);
        btnpagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpagarActionPerformed(evt);
            }
        });

        lblEmpleado.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblEmpleado.setText("ID Empleado:");
        lblEmpleado.setAlignmentX(0.5F);
        lblEmpleado.setAlignmentY(0.0F);

        lblclientes1.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblclientes1.setText("Método de pago: ");
        lblclientes1.setAlignmentX(0.5F);
        lblclientes1.setAlignmentY(0.0F);

        cbmetodo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        cbmetodo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Efectivo", "Débito", "Crédito", "Cheque", "Prepago" }));
        cbmetodo.setBorder(null);
        cbmetodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbmetodoActionPerformed(evt);
            }
        });

        lbltotal1.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbltotal1.setText("Importe:");

        txtbuscar1.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtbuscar1.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtbuscar1.setMinimumSize(new java.awt.Dimension(28, 11));
        txtbuscar1.setPreferredSize(new java.awt.Dimension(11, 28));

        lbltotal2.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbltotal2.setText("Cambio:");

        lbltotalfinal2.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lbltotalfinal2.setText("0.0");

        txtidempleado.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtidempleado.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtidempleado.setMinimumSize(new java.awt.Dimension(28, 11));
        txtidempleado.setPreferredSize(new java.awt.Dimension(11, 28));

        txtbuscar3.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtbuscar3.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtbuscar3.setMinimumSize(new java.awt.Dimension(28, 11));
        txtbuscar3.setPreferredSize(new java.awt.Dimension(11, 28));

        lblclientes2.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblclientes2.setText("ID Factura:");
        lblclientes2.setAlignmentX(0.5F);
        lblclientes2.setAlignmentY(0.0F);

        txtidfacturas.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtidfacturas.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtidfacturas.setMinimumSize(new java.awt.Dimension(28, 11));
        txtidfacturas.setPreferredSize(new java.awt.Dimension(11, 28));

        btncargardatos.setBackground(new java.awt.Color(240, 129, 15));
        btncargardatos.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btncargardatos.setForeground(new java.awt.Color(255, 255, 255));
        btncargardatos.setText("Cargar Datos");
        btncargardatos.setActionCommand("buscar");
        btncargardatos.setAlignmentX(0.5F);
        btncargardatos.setBorder(null);
        btncargardatos.setContentAreaFilled(false);
        btncargardatos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncargardatos.setOpaque(true);
        btncargardatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncargardatosActionPerformed(evt);
            }
        });

        lblclientes3.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        lblclientes3.setText("Cantidad:");
        lblclientes3.setAlignmentX(0.5F);
        lblclientes3.setAlignmentY(0.0F);

        txtcantidad.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        txtcantidad.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtcantidad.setMinimumSize(new java.awt.Dimension(28, 11));
        txtcantidad.setPreferredSize(new java.awt.Dimension(11, 28));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(177, 177, 177)
                        .addComponent(btnvaciar, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnpagar, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(189, 189, 189))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblproductos)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addGap(453, 453, 453)
                                .addComponent(lbltotal1)
                                .addGap(18, 18, 18)
                                .addComponent(txtbuscar1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(360, 360, 360)
                                .addComponent(lbltotalfinal2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblsucursal)
                                .addGap(44, 44, 44)
                                .addComponent(cBoxsucursal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(lblclientes)
                                .addGap(44, 44, 44)
                                .addComponent(txtidclientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(lblEmpleado)
                                .addGap(18, 18, 18)
                                .addComponent(txtidempleado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lbltotal)
                                        .addGap(166, 166, 166)
                                        .addComponent(lbltotalfinal))
                                    .addComponent(lbltotal2)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lblclientes1)
                                        .addGap(18, 18, 18)
                                        .addComponent(cbmetodo, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(90, 90, 90)
                                        .addComponent(lblclientes2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtidfacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(35, 35, 35)
                                        .addComponent(btncargardatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(txtbuscar3, javax.swing.GroupLayout.PREFERRED_SIZE, 653, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblclientes3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(32, 32, 32))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblsucursal)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(cBoxsucursal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmpleado)
                    .addComponent(txtidempleado, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblclientes)
                    .addComponent(txtidclientes, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbmetodo, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblclientes1)
                    .addComponent(lblclientes2)
                    .addComponent(txtidfacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncargardatos, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblclientes3)
                        .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtbuscar3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addComponent(lblproductos)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lbltotalfinal)
                        .addGap(18, 18, 18)
                        .addComponent(lbltotalfinal2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lbltotal, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbltotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lbltotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtbuscar1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 9, Short.MAX_VALUE)))
                        .addGap(26, 26, 26)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnvaciar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnpagar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );

        iconLogoTienda1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/LOGO (9).png"))); // NOI18N

        btnSalir1.setBackground(new java.awt.Color(255, 255, 255));
        btnSalir1.setFont(new java.awt.Font("Century Gothic", 0, 20)); // NOI18N
        btnSalir1.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/cerrar-sesion (1).png"))); // NOI18N
        btnSalir1.setActionCommand("buscar");
        btnSalir1.setAlignmentX(0.5F);
        btnSalir1.setBorder(null);
        btnSalir1.setContentAreaFilled(false);
        btnSalir1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir1.setOpaque(true);

        datehoy.setDateFormatString("yyyy-MM-dd");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(iconUsuario)
                        .addGap(26, 26, 26)
                        .addComponent(lblUsuario)
                        .addGap(41, 41, 41)
                        .addComponent(btnSalir1)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(182, 182, 182)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(89, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(lblSucursal)
                                .addGap(65, 65, 65)
                                .addComponent(iconFecha)
                                .addGap(8, 8, 8)
                                .addComponent(lblFecha)
                                .addGap(35, 35, 35))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(datehoy, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(122, 122, 122))))))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(790, Short.MAX_VALUE)
                    .addComponent(iconLogoTienda1)
                    .addContainerGap(790, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(iconFecha)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblSucursal)
                                .addComponent(lblFecha))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(iconUsuario)
                            .addComponent(lblUsuario)
                            .addComponent(btnSalir1))
                        .addGap(10, 10, 10)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(datehoy, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(32, Short.MAX_VALUE)
                    .addComponent(iconLogoTienda1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(855, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1729, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 949, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPuntoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPuntoVentaActionPerformed

    }//GEN-LAST:event_btnPuntoVentaActionPerformed

    private void btnProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductosActionPerformed

    }//GEN-LAST:event_btnProductosActionPerformed

    private void btnProveedoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProveedoresActionPerformed

    }//GEN-LAST:event_btnProveedoresActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed

    }//GEN-LAST:event_btnClientesActionPerformed

    private void btnRecursosHumanosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecursosHumanosActionPerformed

    }//GEN-LAST:event_btnRecursosHumanosActionPerformed

    private void btnReportesVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportesVentaActionPerformed

    }//GEN-LAST:event_btnReportesVentaActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed

    }//GEN-LAST:event_jButton7ActionPerformed

    private void btnvaciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvaciarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnvaciarActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarActionPerformed

    private void cBoxsucursalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cBoxsucursalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cBoxsucursalActionPerformed

    private void btnpagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpagarActionPerformed

    }//GEN-LAST:event_btnpagarActionPerformed

    private void cbmetodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbmetodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbmetodoActionPerformed

    private void btncargardatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncargardatosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btncargardatosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PuntoDeVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PuntoDeVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PuntoDeVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PuntoDeVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PuntoDeVenta().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnClientes;
    public javax.swing.JButton btnProductos;
    public javax.swing.JButton btnProveedores;
    public javax.swing.JButton btnPuntoVenta;
    public javax.swing.JButton btnRecursosHumanos;
    public javax.swing.JButton btnReportesVenta;
    public javax.swing.JButton btnSalir1;
    public javax.swing.JButton btnagregar;
    public javax.swing.JButton btncargardatos;
    public javax.swing.JButton btnpagar;
    public javax.swing.JButton btnvaciar;
    public javax.swing.JComboBox cBoxsucursal;
    public javax.swing.JComboBox cbmetodo;
    public com.toedter.calendar.JDateChooser datehoy;
    private javax.swing.JLabel iconClientes;
    private javax.swing.JLabel iconFecha;
    private javax.swing.JLabel iconLogoTienda1;
    private javax.swing.JLabel iconProductos;
    private javax.swing.JLabel iconProveedores;
    private javax.swing.JLabel iconPuntoVenta;
    private javax.swing.JLabel iconRecursosHumanos;
    private javax.swing.JLabel iconReportesVenta;
    private javax.swing.JLabel iconUsuario;
    public javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblContacto;
    private javax.swing.JLabel lblCorreo;
    public javax.swing.JLabel lblEmpleado;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblSucursal;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblUsuario;
    public javax.swing.JLabel lblclientes;
    public javax.swing.JLabel lblclientes1;
    public javax.swing.JLabel lblclientes2;
    public javax.swing.JLabel lblclientes3;
    public javax.swing.JLabel lblproductos;
    public javax.swing.JLabel lblsucursal;
    private javax.swing.JLabel lbltotal;
    private javax.swing.JLabel lbltotal1;
    private javax.swing.JLabel lbltotal2;
    public javax.swing.JLabel lbltotalfinal;
    public javax.swing.JLabel lbltotalfinal2;
    public javax.swing.JTable tbproductos;
    public javax.swing.JTextField txtbuscar1;
    public javax.swing.JTextField txtbuscar3;
    public javax.swing.JTextField txtcantidad;
    public javax.swing.JTextField txtidclientes;
    public javax.swing.JTextField txtidempleado;
    public javax.swing.JTextField txtidfacturas;
    // End of variables declaration//GEN-END:variables
}
